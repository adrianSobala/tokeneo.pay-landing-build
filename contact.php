<?php

require '../vendor/autoload.php';

use Aws\Ses\SesClient;
use Aws\Credentials\Credentials;
use Aws\Exception\AwsException;

['recipient' => $recipient, 'sender' => $sender, 'RECAPTCHA_SECRET' => $RECAPTCHA_SECRET, 'region' => $region, 'awsKey' => $awsKey, 'awsSecret' => $awsSecret] = include(__DIR__ . '/../env.php');

function reCaptchaVerified($recaptchaResponse)
{
    global $RECAPTCHA_SECRET;

    $params = [
        'secret' => $RECAPTCHA_SECRET,
        'response' => $recaptchaResponse
    ];

    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?' . http_build_query($params));

    $responseData = json_decode($verifyResponse, true);

    if (isset($responseData['success']) && $responseData['success'] == true) {
        return true;
    }

    return false;
}

if (isset($_POST)) {

    $companyName = isset($_POST['companyName']) ? htmlspecialchars($_POST['companyName']) : null;
    $shopName = isset($_POST['shopName']) ? htmlspecialchars($_POST['shopName']) : null;
    $email = isset($_POST['email']) ? htmlspecialchars($_POST['email']) : null;
    $person = isset($_POST['person']) ? htmlspecialchars($_POST['person']) : null;
    $shopUrl = isset($_POST['shopUrl']) ? htmlspecialchars($_POST['shopUrl']) : null;
    $phone = isset($_POST['phone']) ? htmlspecialchars($_POST['phone']) : null;
    $message = isset($_POST['message']) ? htmlspecialchars($_POST['message']) : null;
    $companyAddress = isset($_POST['companyAddress']) ? htmlspecialchars($_POST['companyAddress']) : null;
    $tin = isset($_POST['tin']) ? htmlspecialchars($_POST['tin']) : null;
    $token = isset($_POST['token']) ? htmlspecialchars($_POST['token']) : null;

    if (!reCaptchaVerified($token)) exit(json_encode(['status' => false, 'message' => 'Nieprawidłowy token.']));

    if (empty($companyName)) exit(json_encode(['status' => false, 'message' => 'Nazwa firmy jest obowiązkowa.']));
    if (empty($email)) exit(json_encode(['status' => false, 'message' => 'Adres e-mail jest obowiązkowy.']));
    if (empty($person)) exit(json_encode(['status' => false, 'message' => 'Osoba kontaktowa jest obowiązkowa.']));
    if (empty($phone)) exit(json_encode(['status' => false, 'message' => 'Telefon kontaktowy jest obowiązkowy.']));

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) exit(json_encode(['status' => false, 'message' => 'Nieprawidłowy adres e-mail.']));

    $subject = 'TokeneoPay.com - Formularz kontaktowy';

    $wiadomosc = '<html> 
    <head> 
    	<title>TokeneoPay.com - Formularz kontaktowy</title> 
    </head>
    <body>
    	<p>
    		<b>Nazwa firmy:</b> ' . $companyName . '<br>
    		<b>Osoba kontaktowa:</b> ' . $person . '<br>
    		<b>Adres e-mail:</b> ' . $email . '<br>
    		<b>Telefon kontaktowy:</b> ' . $phone . '<br>
    		' . (!empty($shopName) ? '<b>Nazwa sklepu internetowego:</b> ' . $shopName . '<br>' : null) . '
    		' . (!empty($shopUrl) ? '<b>URL sklepu internetowego:</b> ' . $shopUrl . '<br>' : null) . '
    		' . (!empty($companyAddress) ? '<b>Adres firmowy:</b> ' . $companyAddress . '<br>' : null) . '
    		' . (!empty($tin) ? '<b>TIN:</b> ' . $tin . '<br>' : null) . '
    		<br>
    		<b>Treść wiadomości:</b><br>
    		' . $message . '
    	</p>
    </body>
    </html>';

    $credentials = new Credentials($awsKey, $awsSecret);

    $SesClient = new SesClient([
        'credentials' => $credentials,
        'version' => '2010-12-01',
        'region' => $region
    ]);

    $char_set = 'UTF-8';

    try {
        mb_internal_encoding('UTF-8');
        $result = $SesClient->sendEmail([
            'Destination' => [
                'ToAddresses' => [$recipient],
            ],
            'ReplyToAddresses' => [$email],
            'Source' => mb_encode_mimeheader($sender, 'UTF-8', 'Q') . ' <' . $sender . '>',
            'Message' => [
                'Body' => [
                    'Html' => [
                        'Charset' => $char_set,
                        'Data' => $wiadomosc,
                    ]
                ],
                'Subject' => [
                    'Charset' => $char_set,
                    'Data' => $subject,
                ],
            ]
        ]);
        $messageId = $result['MessageId'];
        exit(json_encode(['status' => true, 'message' => 'Wiadomość została wysłana, dziękujemy.']));
    } catch (AwsException $e) {
        //echo $e->getMessage();
        //echo("The email was not sent. Error message: ".$e->getAwsErrorMessage()."\n");
        exit(json_encode(['status' => false, 'message' => 'Wystąpił problem z wysyłką wiadomości, spróbuj ponownie później.']));
    }

}

exit(json_encode(['status' => false, 'message' => 'Błąd.']));
